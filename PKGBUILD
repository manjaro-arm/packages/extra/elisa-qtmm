# Maintainer: Dan Johansen <strit@manjaro.org>
# Arch Maintainer: Antonio Rojas <arojas@archlinux.org>

pkgname=elisa-qtmm
_pkgname=elisa
pkgver=23.08.4
pkgrel=1
pkgdesc='A simple music player aiming to provide a nice experience for its users'
url='https://apps.kde.org/elisa/'
arch=(x86_64 aarch64)
license=(LGPL3)
groups=(kde-applications kde-multimedia)
depends=(baloo5 kirigami2 kdeclarative5) # upnp-player-qt
provides=('elisa')
conflicts=('elisa')
makedepends=(extra-cmake-modules kdoctools5)
source=(https://download.kde.org/stable/release-service/$pkgver/src/$_pkgname-$pkgver.tar.xz{,.sig}
        https://invent.kde.org/multimedia/elisa/-/merge_requests/528.patch)
sha256sums=('aac8bbd15c8ae7740ae46c48e968d8b950a8f8aaa9081736c0cf08e5c99d5143'
            'SKIP'
            'b3a4d11dd6ddfe6d8cb0df2608c0b1e74dc8459635203370c3b2ec9bf9db190d')
validpgpkeys=(CA262C6C83DE4D2FB28A332A3A6A4DB839EAA6D7  # Albert Astals Cid <aacid@kde.org>
              F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87  # Christoph Feck <cfeck@kde.org>
              D81C0CB38EB725EF6691C385BB463350D6EF31EF) # Heiko Becker <heiko.becker@kde.org>

prepare() {
  cd $_pkgname-$pkgver
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ "${src}" = *.patch ]] || continue
    echo "Applying patch ${src}..."
    patch -Np1 < "../${src}"
  done
}

build() {
  cmake -B build -S $_pkgname-$pkgver \
    -DBUILD_TESTING=OFF 
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
}
